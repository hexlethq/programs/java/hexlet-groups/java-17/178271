package exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    public static void beforeAll() {
        app = App.getApp();
        app.start();
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    public static void afterAll() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testNewUserPositive() {
        HttpResponse<String> response = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "Vasya")
                .field("lastName", "Pupkin")
                .field("email", "mail@gmail.com")
                .field("password", "1234567")
                .asString();

        User user = new QUser()
                .lastName.equalTo("Pupkin")
                .findOne();

        assertThat(response.getStatus()).isEqualTo(302);
        assertThat(user.getFirstName()).isEqualTo("Vasya");
        assertThat(user.getLastName()).isEqualTo("Pupkin");
        assertThat(user.getEmail()).isEqualTo("mail@gmail.com");
        assertThat(user.getPassword()).isEqualTo("1234567");
    }

    @Test
    void testNewUserNegative() {
        HttpResponse<String> response = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "")
                .field("lastName", "Pupkin")
                .field("email", "mail@somemail.com")
                .field("password", "7654321")
                .asString();

        User user = new QUser()
                .email.equalTo("mail@somemail.com")
                .findOne();

        String body = response.getBody();

        assertThat(response.getStatus()).isEqualTo(422);
        assertThat(user).isNull();
        assertThat(body).contains("Pupkin");
        assertThat(body).contains("mail@somemail.com");
        assertThat(body).contains("Имя не должно быть пустым");
    }
    // END
}
