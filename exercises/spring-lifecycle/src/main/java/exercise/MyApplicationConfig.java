package exercise;

import java.time.LocalDateTime;

import exercise.daytimes.Daytime;
import exercise.daytimes.Morning;
import exercise.daytimes.Day;
import exercise.daytimes.Evening;
import exercise.daytimes.Night;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


// BEGIN
@Configuration
public class MyApplicationConfig {
    @Bean("daytime-for-meal")
    public Daytime initBean() {
        LocalDateTime currentTime = LocalDateTime.now();
        int currentHour = currentTime.getHour();

        switch ((int) currentHour / 6) {
            case 1:
                return new Morning();
            case 2:
                return new Day();
            case 3:
                return new Evening();
            default:
                return new Night();
        }
    }
}
// END
