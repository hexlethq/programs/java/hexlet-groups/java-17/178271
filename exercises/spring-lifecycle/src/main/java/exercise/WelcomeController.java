package exercise;

import org.springframework.web.bind.annotation.GetMapping;
import exercise.daytimes.Daytime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RestController;

// BEGIN
@RestController
public class WelcomeController {
    @Autowired
    private Daytime dayTime;

    @Autowired
    private Meal meal;

    @GetMapping("/daytime")
    public String getMealByTime() {
        String time = dayTime.getName();
        String mealName = meal.getMealForTime(time);
        return String.format("It is %s  now. Enjoy your %s", time, mealName);
    }
}
// END
