package exercise.controllers;

import io.javalin.http.Handler;
import java.util.List;
import java.util.Map;
import io.javalin.core.validation.Validator;
import io.javalin.core.validation.ValidationError;
import io.javalin.core.validation.JavalinValidation;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

import exercise.domain.User;
import exercise.domain.query.QUser;

public final class UserController {

    public static Handler listUsers = ctx -> {

        List<User> users = new QUser()
            .orderBy()
                .id.asc()
            .findList();

        ctx.attribute("users", users);
        ctx.render("users/index.html");
    };

    public static Handler showUser = ctx -> {
        long id = ctx.pathParamAsClass("id", Long.class).getOrDefault(null);

        User user = new QUser()
            .id.equalTo(id)
            .findOne();

        ctx.attribute("user", user);
        ctx.render("users/show.html");
    };

    public static Handler newUser = ctx -> {

        ctx.attribute("errors", Map.of());
        ctx.attribute("user", Map.of());
        ctx.render("users/new.html");
    };

    public static Handler createUser = ctx -> {
        // BEGIN
        Validator<String> firstName = ctx
                .formParamAsClass("firstName", String.class)
                .check(x -> !x.isBlank(), "Name should not be empty!");
        Validator<String> lastName = ctx
                .formParamAsClass("lastName", String.class)
                .check(x -> !x.isBlank(), "Lastname should not be empty!");
        Validator<String> email = ctx
                .formParamAsClass("email", String.class)
                .check(x -> EmailValidator.getInstance().isValid(x), "Email is not valid!");
        Validator<String> password = ctx
                .formParamAsClass("password", String.class)
                .check(StringUtils::isNumeric, "Password should contain numbers only")
                .check(x -> x.length() >= 4, "Password should be longer then 4 digits");

        User user = new User(
                ctx.formParam("firstName"),
                ctx.formParam("lastName"),
                ctx.formParam("email"),
                ctx.formParam("password")
        );

        Map<String, List<ValidationError<?>>> errors = JavalinValidation.collectErrors(
                firstName,
                lastName,
                email,
                password
        );

        if (!errors.isEmpty()) {
            ctx.status(422);
            ctx.attribute("errors", errors);
            ctx.attribute("user", user);
            ctx.render("users/new.html");
            return;
        }

        user.save();
        ctx.sessionAttribute("flash", "Пользователь успешно создан!");
        ctx.redirect("/users");
        // END
    };
}
