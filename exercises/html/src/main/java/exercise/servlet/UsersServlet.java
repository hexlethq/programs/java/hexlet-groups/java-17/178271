package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {
    private static String htmlTop = "<!DOCTYPE html>"
            + "<html lang=\"ru\">"
            + "<head>"
            + "<meta charset=\"UTF-8\">"
            + "<title>Users</title>"
            + "<link rel=\"stylesheet\" href=\"mysite.css\">"
            + "</head>"
            + "<body>"
            + "<table>";

    private static String htmlBottom = "</table></body></html>";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper objectMapper = new ObjectMapper();
        List<Map<String, String>> users = objectMapper.readValue(Paths.get("src/main/resources/users.json").toFile(), List.class);
        return users;
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        StringBuilder body = new StringBuilder();
        body.append(htmlTop);

        for (Map user : users) {
            body.append("<tr>"
                    + "<td>" + user.get("id") + "</td>"
                    + "<td>"
                    + "<a href=\"/users/" + user.get("id") +"\">"
                    + user.get("firstName") + " " + user.get("lastName")
                    + "</a>"
                    + "</td>"
                    + "</tr>");
        }

        body.append(htmlBottom);

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(body.toString());

        return;
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        boolean userFound = false;
        StringBuilder body = new StringBuilder();
        body.append(htmlTop);

        for (Map user : users) {
            if (user.containsValue(id)) {
                body.append("<tr>"
                        + "<td>" + user.get("id") + "</td>"
                        + "<td>" + user.get("firstName") + "</td>"
                        + "<td>" + user.get("lastName") + "</td>"
                        + "<td>" + user.get("email") + "</td>"
                        + "</tr>");
                userFound = true;
            }
        }

        if (!userFound) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "User not found");
            return;
        }

        body.append(htmlBottom);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(body.toString());
        // END
    }
}
