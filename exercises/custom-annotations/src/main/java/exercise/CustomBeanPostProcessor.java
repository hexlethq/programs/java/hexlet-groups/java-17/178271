package exercise;

import java.lang.reflect.Proxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private static final String LOGGER_OUTPUT = "Was called method: %s() with arguments: %s";
    private Map<String, String> beanData = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            Inspect inspect = bean.getClass().getAnnotation(Inspect.class);
            beanData.put(beanName, inspect.level());
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if (!beanData.containsKey(beanName)) {
            return bean;
        }

        String loggerLevel = beanData.get(beanName);
        Logger logger = LoggerFactory.getLogger(beanName);

        Object proxyInstance = Proxy.newProxyInstance(
                bean.getClass().getClassLoader(),
                bean.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    String message = String.format(LOGGER_OUTPUT, method.getName(), Arrays.toString(args));

                    if (loggerLevel.equals("info")) {
                        logger.info(message);
                        return method.invoke(bean, args);
                    }

                    logger.debug(message);
                    return method.invoke(bean, args);
                });
        return proxyInstance;
    }
}
// END
