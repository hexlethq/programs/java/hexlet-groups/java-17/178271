package exercise.controller;

import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;
import exercise.CityNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public Map<String, String> getWeatherByCityId(@PathVariable(name = "id") long id) throws JsonProcessingException {
        City city = cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City not found"));

        String cityName = city.getName();
        String weather = weatherService.getWeather(cityName);
        Map<String, String> cityWeather = new ObjectMapper().readValue(weather, HashMap.class);

        return cityWeather;
    }

    @GetMapping(path = "/search")
    public List<Map<String, String>> getWeatherByCities(@RequestParam(name = "name", required = false) String name)
            throws JsonProcessingException {

        if (name != null) {
            return getWeatherListByCityName(name);
        }

        return getTotalWeatherList();
    }

    private List<Map<String, String>> getWeatherListByCityName(String cityName) throws JsonProcessingException {
        List<Map<String, String>> weatherList = new ArrayList<>();
        List<City> cities =  cityRepository.findAllByNameStartsWithIgnoreCase(cityName);

        for (City city : cities) {
            Map<String, String> cityWeather = getCityWeather(city);
            weatherList.add(cityWeather);
        }

        return weatherList;
    }

    private List<Map<String, String>> getTotalWeatherList() throws JsonProcessingException {
        List<Map<String, String>> weatherList = new ArrayList<>();
        List<City> cities = cityRepository.findAllByOrderByNameAsc();

        for (City city : cities) {
            Map<String, String> cityWeather = getCityWeather(city);
            weatherList.add(cityWeather);
        }

        return weatherList;
    }

    private Map<String, String> getCityWeather(City city) throws JsonProcessingException {
        String cityName = city.getName();
        String weather = weatherService.getWeather(cityName);
        return new ObjectMapper().readValue(weather, HashMap.class);
    }
    // END
}

