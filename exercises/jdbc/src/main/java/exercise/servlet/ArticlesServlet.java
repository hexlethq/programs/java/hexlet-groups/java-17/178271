package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;

import org.apache.commons.lang3.ArrayUtils;

import exercise.Article;
import exercise.TemplateEngineUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;



public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                try {
                    showArticles(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                try {
                    showArticle(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException, ServletException, SQLException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        String query = "SELECT * FROM articles ORDER BY id LIMIT 10 OFFSET ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        String page = request.getParameter("page");
        int currentPage = Objects.equals(page, null) ? 1 : Integer.parseInt(page);
        preparedStatement.setInt(1, (currentPage * 10) - 10);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Article> articles = new ArrayList<>();
        while (resultSet.next()) {
            Article article = new Article();
            article.setId(resultSet.getLong("id"));
            article.setBody(resultSet.getString("body"));
            article.setTitle(resultSet.getString("title"));
            articles.add(article);
        }

        request.setAttribute("articles", articles);
        request.setAttribute("currentPage", currentPage);
        preparedStatement.close();
        resultSet.close();
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
                 throws IOException, ServletException, SQLException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        int id = Integer.parseInt(getId(request));
        if (id > 100) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        String query = "SELECT title, body FROM articles WHERE ID = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.first();
        request.setAttribute("title", resultSet.getString(1));
        request.setAttribute("body", resultSet.getString(2));
        preparedStatement.close();
        resultSet.close();
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}
