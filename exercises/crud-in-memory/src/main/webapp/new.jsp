<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add new user</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
            crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <a href="/users">Все пользователи</a>
            <!-- BEGIN -->
            <form action="/users/new" method="post" class="form">

                <a href="/users">Все пользователи</a>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Имя" value="${user.getOrDefault("firstName", "")}" name="firstName" />
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Фамилия" value="${user.getOrDefault("lastName", "")}" name="lastName" />
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="E-mail" value="${user.getOrDefault("email", "")}" name="email" />
                </div>

                <div>${error}</div>
                <button class="btn btn-primary" type="submit">Создать</button>
            </form>
            <!-- END -->
        </div>
    </body>
</html>
